package com.exceptions;

public class SecurityUserException extends Exception {
    public SecurityUserException(){super();}
    public SecurityUserException(String msg){super(msg);}
}
