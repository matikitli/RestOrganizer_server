package com.exceptions;

public class SecurityActivityException extends Exception {
    public SecurityActivityException(){super();}
    public SecurityActivityException(String msg){
        super(msg);
    }
}
