package com.repositories;

import com.models.Activity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.List;

public interface ActivityRepo extends JpaRepository<Activity, Long> {
    /*
    @Query(value = "SELECT * FROM USER_OBJECT WHERE ID  = ?1", nativeQuery = true)
    Collection<Activity> findAllByCreatorId(Long id);
    */
    //Collection<Activity> findByCreatorId(Long creatorId);

    List<Activity> findAllByUser(String user);

}
