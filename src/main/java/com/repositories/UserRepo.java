package com.repositories;

import com.models.UserObject;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepo extends JpaRepository<UserObject,Long> {
    UserObject findByUsername(String username);

}
