package com.repositories;

import com.models.Activity;
import com.models.UserPref;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserPrefRepo extends JpaRepository<UserPref, Long> {

    UserPref findById(long id);
    UserPref findByUser(String user);

}
