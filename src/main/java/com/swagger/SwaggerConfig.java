package com.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.paths.AbstractPathProvider;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@EnableSwagger2
@Configuration
public class SwaggerConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("controllers")
                .select()
                .apis(RequestHandlerSelectors.any())
                //TODO: improve this antPattern mb to regex
                .paths(PathSelectors.ant("/api/*/*"))
                .build()
                .apiInfo(getApiInfo());
    }

    @Bean
    public Docket apiAll(){
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("all")
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build()
                .apiInfo(getApiInfo());
    }

    private ApiInfo getApiInfo(){
        return new ApiInfo("REST ORGANIZER",
                "rest-full server api",
                "v1",
                "",
                "Mateusz Kitlinski",
                "",
                "");
    }

    private static class VersioningPathProvider extends AbstractPathProvider {
        private String basePath;

        public VersioningPathProvider(String basePath) {
            this.basePath = basePath;
        }

        @Override
        protected String applicationPath() {
            return basePath;
        }

        @Override
        protected String getDocumentationPath() {
            return "/";
        }
    }

}
