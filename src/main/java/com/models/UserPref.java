package com.models;

import net.bytebuddy.implementation.bind.annotation.Default;
import org.springframework.stereotype.Repository;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Entity
@Table(name = "userPref")
public class UserPref {

    @Id
    @Column(name = "id")
    private long id;

    @Column(name="user")
    private String user;

    @Column(name = "inactiveDays")
    @Max(31) @Min(3)
    private int inactiveDays=7;

    public UserPref(){}

    public UserPref(long id){
        this.id=id;
    }
    public UserPref(long id, String user){
        this.id=id;
        this.user=user;
    }





    public long getId() {
        return id;
    }

    public String getUser() {
        return user;
    }

    public int getInactiveDays() {
        return inactiveDays;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setInactiveDays(int inactiveDays) {
        this.inactiveDays = inactiveDays;
    }
}
