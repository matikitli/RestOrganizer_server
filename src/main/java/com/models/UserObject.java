package com.models;

import com.security.securityEnums.RoleEnum;

import javax.persistence.*;

@Entity
public class UserObject {

    @Id @GeneratedValue(strategy = GenerationType.AUTO) private long id;
    @Column(name = "username",unique = true) private String username;
    @Column(name = "role") @Enumerated(EnumType.STRING) private RoleEnum role;
    @Column(name = "email", unique = true) private String email;
    @Column (name = "password") private String password;

    public long getId() {
        return id;
    }

    public RoleEnum getRole() { return role; }

    public void setRole(RoleEnum role) { this.role = role; }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public EmailObject getEmail(String email) {
        EmailObject emailObject = new EmailObject(email);
        return emailObject;
    }

    public String getEmailString() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isOwnerOfActivity(Activity activity){
        if(activity.getCreator_id() == this.id) return true;
        else return false;
    }


}
