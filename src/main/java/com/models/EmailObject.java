package com.models;

public class EmailObject {

    private final String prefix;
    private final String suffix;

    public EmailObject(String email){
        String[] splited = email.split("@");
        this.prefix=splited[0];
        this.suffix=splited[1];
    }

    public String getPrefix() {
        return prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    @Override
    public String toString() {
        return prefix+"@"+suffix;
    }
}
