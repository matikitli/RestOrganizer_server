package com.models;

public class UsernamePasswordPack {
    private final String username;
    private final String password;

    public UsernamePasswordPack(String username,String password){
        this.username=username;
        this.password=password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
