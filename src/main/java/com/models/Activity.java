package com.models;


import com.models.enums.TypeEnum;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "activity")
public class Activity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    private long creator_id;

    @Column(name = "user")
    private String user;

    @Column(name = "title")
    //@NotNull
    private String title;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private TypeEnum typeEnum;

    @Column(name = "description")
    private String description;

    @Column(name = "rate")
    @Min(1) @Max(10)
    //@NotNull
    private int rate;

    @Column(name = "minutes")
    //@NotNull
    private int minutes;

    @Column(name = "link")
    private String link;

    @Column(name = "learned")
    private int percentLearned=0;

    @Column(name = "data")
    private Date dateCreated;


    public Activity(){}

    public Activity(String user,String title,TypeEnum typeEnum,String description,int rate,int minutes,String link,int percentLearned){
        this.user=user;
        this.title=title;
        this.typeEnum=typeEnum;
        this.description=description;
        this.rate=rate;
        this.minutes=minutes;
        this.link=link;
        this.percentLearned=percentLearned;
        this.dateCreated=new Date();


    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public TypeEnum getTypeEnum() {
        return typeEnum;
    }

    public void setTypeEnum(TypeEnum typeEnum) {
        this.typeEnum = typeEnum;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getRate() {
        return rate;
    }

    public int getMinutes() {
        return minutes;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public int getPercentLearned() {
        return percentLearned;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public long getId() {
        return id;
    }

    public long getCreator_id() {
        return creator_id;
    }

    public void setCreator_id(long creator_id) {
        this.creator_id = creator_id;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public void setPercentLearned(int percentLearned) {
        this.percentLearned = percentLearned;
    }

    //another fnc
    public boolean isUserActiveInActivity(long id){
        if(this.creator_id==id){return true;}
        else{ return false;}
    }
}
