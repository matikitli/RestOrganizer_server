package com.security.service;

import com.models.UserObject;
import com.models.UsernamePasswordPack;
import org.springframework.util.Base64Utils;

import java.util.Collection;

public interface UserService {
    UserObject getUserByUsername(String username);
    Collection<UserObject> getAllUsers();


    static UsernamePasswordPack decodateUserPack(String auth){
        String[] split = new String(Base64Utils.decode(auth.getBytes())).split(":");
        String usr = split[0];
        String psw = split[1];
        return new UsernamePasswordPack(usr,psw);
    }

}
