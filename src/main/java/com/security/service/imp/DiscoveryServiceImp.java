package com.security.service.imp;

import com.models.UserObject;
import com.security.service.DiscoveryService;
import com.security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class DiscoveryServiceImp implements DiscoveryService {

    private final UserService userService;

    @Autowired
    public DiscoveryServiceImp(UserService userService){
        this.userService=userService;
    }

    @Override
    public UserObject getLoggedUser() {
        UserObject userFromHolder = (UserObject) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userFromHolder;
    }
}
