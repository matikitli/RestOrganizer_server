package com.security.service.imp;

import com.models.UserObject;
import com.repositories.UserRepo;
import com.security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class UserServiceImp implements UserService {

    private final UserRepo userRepository;

    @Autowired
    public UserServiceImp(UserRepo userRepo){
        this.userRepository=userRepo;
    }

    @Override
    public UserObject getUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public Collection<UserObject> getAllUsers() {
        return userRepository.findAll();
    }
}
