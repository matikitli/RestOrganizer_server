package com.security.service;

import com.models.UserObject;

public interface DiscoveryService {
    UserObject getLoggedUser();
}
