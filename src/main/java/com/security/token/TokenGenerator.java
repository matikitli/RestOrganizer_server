package com.security.token;

import com.models.UserObject;
import com.security.configuration.WebSecurityConfig;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.crypto.codec.Base64;

import java.util.Date;

public class TokenGenerator {

    private TokenGenerator(){}

    public static String generateToken(UserObject user) {
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
        long currentTime = System.currentTimeMillis();
        long expiredTime = currentTime + (1 * 24 * 60 * 60 * 1000);
        byte[] signingKey = Base64.encode(WebSecurityConfig.SECRET.getBytes());

        JwtBuilder builder = Jwts.builder().setId(user.getUsername())
                .setExpiration(new Date(expiredTime))
                .setHeaderParam(TokenAuthentication.TYP_KEY,TokenAuthentication.JWT)
                .signWith(signatureAlgorithm, signingKey);

        return builder.compact();

    }
}
