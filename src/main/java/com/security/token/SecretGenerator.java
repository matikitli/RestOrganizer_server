package com.security.token;

import java.security.SecureRandom;

public class SecretGenerator {

    public static final int SECRET_LENGTH = 36;

    public static String getNewSecret() {
        SecureRandom rng = new SecureRandom();
        byte[] secret = new byte[SECRET_LENGTH];
        rng.nextBytes(secret);
        return new String(secret);
    }
}
