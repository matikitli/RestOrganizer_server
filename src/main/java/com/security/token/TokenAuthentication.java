package com.security.token;

import com.models.UserObject;
import com.security.service.UserService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.codec.Base64;

import javax.servlet.http.HttpServletRequest;

public class TokenAuthentication {
    private final String HEADER = "X-AUTH-TOKEN";
    private final String secret;
    static final String TYP_KEY = "typ";
    static final String JWT = "JWT";
    @Autowired
    private UserService userService;


    public TokenAuthentication(String secret){this.secret = secret;}

    UserAuthentication getAuthentication(HttpServletRequest req){
        String token = req.getHeader(HEADER);
        if(token != null){
            UserObject user = parseUserFromToken(token);
            if(user != null){
                return new UserAuthentication(user);
            }
        }
        return null;
    }



    //this parse all information from token
    private UserObject parseUserFromToken(String token){
        Claims claims = Jwts.parser()
                .setSigningKey(Base64.encode(secret.getBytes()))
                .parseClaimsJws(token)
                .getBody();

        String username = claims.getId();
        return userService.getUserByUsername(username);
    }
}

