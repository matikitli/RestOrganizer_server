package com.security.securityEnums;

public enum PrivilegeEnum {
    ADD_POST,           //user,admin
    EDIT_POST,      //user,admin
    DEL_POST,       //user,admin
    GET_POST,           //user,admin
    DEL_NOTOWN_POST     //admin
}
