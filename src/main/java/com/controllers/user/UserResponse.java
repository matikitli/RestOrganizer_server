package com.controllers.user;

import com.models.UserObject;
import com.security.securityEnums.RoleEnum;

public class UserResponse {

    private long id;
    private String username;
    private RoleEnum role;
    private String email;

    public UserResponse(){

    }

    public UserResponse(long id, String username, RoleEnum role, String email) {
        this.id = id;
        this.username = username;
        this.role = role;
        this.email = email;
    }

    public UserResponse(UserObject userObject){
        this.id=userObject.getId();
        this.username=userObject.getUsername();
        this.role=userObject.getRole();
        this.email=userObject.getEmailString();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public RoleEnum getRole() {
        return role;
    }

    public void setRole(RoleEnum role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
