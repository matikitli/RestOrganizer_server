package com.controllers.user;

import com.models.UserObject;
import com.models.UsernamePasswordPack;
import com.repositories.UserRepo;
import com.security.securityEnums.RoleEnum;
import com.security.service.DiscoveryService;
import com.security.service.UserService;
import com.security.service.imp.UserServiceImp;
import com.security.token.TokenGenerator;
import com.security.token.TokenResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.naming.AuthenticationException;
import java.util.Collection;

@RestController
@RequestMapping("/api/user")
public class UserReaderController {

    @Autowired
    private UserRepo userRepository;

    private final BCryptPasswordEncoder encoder;
    private final DiscoveryService discoveryService;
    private final UserService userService;

    @Autowired
    public UserReaderController(DiscoveryService discoveryService, UserService userService, BCryptPasswordEncoder encoder){
        this.discoveryService=discoveryService;
        this.userService=userService;
        this.encoder=encoder;
    }


    @RequestMapping(path = "/login",method = RequestMethod.GET)
    @ResponseBody
    public TokenResponse login(@RequestHeader(value = "Authorization") String authorization) throws AuthenticationException{
        UsernamePasswordPack usernamePasswordPack = UserService.decodateUserPack(authorization);
        UserObject user = userRepository.findByUsername(usernamePasswordPack.getUsername());
        String token = TokenGenerator.generateToken(user);
        return new TokenResponse(token);
    }

    @RequestMapping(path = "/current",method = RequestMethod.GET)
    @ResponseBody
    public UserObject currentUser(){return discoveryService.getLoggedUser();}

    @RequestMapping(path = "/all",method = RequestMethod.GET)
    @ResponseBody
    public Collection<UserObject> allUsers() throws Exception {
        if(discoveryService.getLoggedUser().getRole().equals(RoleEnum.ROLE_ADMIN)) {
            userRepository.findAll();
        }
        else throw new Exception();
        return null;
    }

    @RequestMapping(path = "/{n}",method = RequestMethod.GET)
    @ResponseBody
    public UserObject findById(@PathVariable("n") Long id) throws Exception {

        if(discoveryService.getLoggedUser().getRole().equals(RoleEnum.ROLE_ADMIN)){
            userRepository.findOne(id);
        }
        else{
            throw new Exception();
        }
        return null;
    }


    @RequestMapping(path = "/findByUsername/{n}", method = RequestMethod.GET)
    @ResponseBody
    public UserResponse findByUsername(@PathVariable("n") String n){
        UserResponse userResponse = new UserResponse(userRepository.findByUsername(n));
        return userResponse;
    }

    @RequestMapping(path = "/{n}/del",method = RequestMethod.DELETE)
    @ResponseBody
    public String deleteUsernameById(@PathVariable("n") Long id){
        UserObject user = userRepository.findOne(id);
        userRepository.delete(id);
        return "User deleted: "+user.getUsername();
    }

}
