package com.controllers.user;


import com.exceptions.SecurityUserException;
import com.models.UserObject;
import com.models.UserPref;
import com.repositories.UserPrefRepo;
import com.repositories.UserRepo;
import com.security.securityEnums.RoleEnum;
import com.security.service.DiscoveryService;
import com.security.token.TokenGenerator;
import com.security.token.TokenResponse;
import org.eclipse.jetty.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.management.relation.Role;

@RestController
@RequestMapping("/api/user")
public class UserWriterController {

    @Autowired
    private UserRepo userRepository;

    @Autowired
    private UserPrefRepo userPrefRepo;

    private final DiscoveryService discoveryService;
    private final BCryptPasswordEncoder encoder;

    @Autowired
    public UserWriterController(DiscoveryService discoveryService,BCryptPasswordEncoder encoder){
        this.discoveryService=discoveryService;
        this.encoder=encoder;
    }

    @RequestMapping(path = "/register", method = RequestMethod.POST)
    @ResponseBody
    public TokenResponse registerUser(@RequestParam String username, @RequestParam String password, @RequestParam(required = false) String role,@RequestParam String email)
    throws SecurityUserException {
        UserObject user = new UserObject();
        if (userRepository.findByUsername(username) != null) {
            throw new SecurityUserException("There is already user with this USERNAME ");
        }
        else {
            user.setUsername(username);

            user.setPassword(encoder.encode(password));
            if (role == null || role.equalsIgnoreCase(RoleEnum.ROLE_USER.toString())) {
                user.setRole(RoleEnum.ROLE_USER);
            } else{
                if(discoveryService.getLoggedUser().getRole().equals(RoleEnum.ROLE_ADMIN))
                user.setRole(RoleEnum.valueOf(role.toUpperCase()));
            }
            user.setEmail(email);
            userRepository.save(user);
            userPrefRepo.save(new UserPref(user.getId(),user.getUsername()));
            String token = TokenGenerator.generateToken(user);
            return new TokenResponse(token);

        }

    }


    @RequestMapping(path = "/current/edit",method = RequestMethod.POST)
    @ResponseBody
    public UserPref currentUser(@RequestParam(required = false) int inactiveDays){
        UserObject current = discoveryService.getLoggedUser();

        UserPref userPref = new UserPref(current.getId());
        userPref.setInactiveDays(inactiveDays);
        userPref.setUser(current.getUsername());
        userPrefRepo.save(userPref);
        return userPref;

    }
}
