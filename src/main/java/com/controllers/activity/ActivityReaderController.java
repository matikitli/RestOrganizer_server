package com.controllers.activity;

import com.exceptions.SecurityActivityException;
import com.models.Activity;
import com.models.UserObject;
import com.repositories.ActivityRepo;
import com.security.service.DiscoveryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(path = "/api/activity")
public class ActivityReaderController {

    @Autowired
    private ActivityRepo activityRepo;
    private final DiscoveryService discoveryService;

    @Autowired
    public ActivityReaderController(DiscoveryService discoveryService,ActivityRepo activityRepo){
        this.discoveryService=discoveryService;
        this.activityRepo=activityRepo;
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    List<Activity> listOfAllActivities(){
        return activityRepo.findAllByUser(discoveryService.getLoggedUser().getUsername());
    }

    @RequestMapping(value = "/{n}",method = RequestMethod.GET)
    @ResponseBody
    Activity findOneById(@PathVariable("n") Long id) throws SecurityActivityException {
        Activity activity = activityRepo.findOne(id);
        if(activity.isUserActiveInActivity(discoveryService.getLoggedUser().getId())){
            return activity;
        }
        else throw new SecurityActivityException("You dont have permission to see this activity");
    }

    @RequestMapping(value = "/today",method = RequestMethod.GET)
    @ResponseBody
    List<Activity> findAllFromToday() throws SecurityActivityException {
        Date now = new Date();
        LocalDate localDate = now.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        List<Activity> activityList = activityRepo.findAllByUser(discoveryService.getLoggedUser().getUsername());
        List<Activity> todayActivities = new ArrayList<>();
        for (Activity a: activityList) {
            LocalDate activityDate = a.getDateCreated().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            if(activityDate.getDayOfMonth()==localDate.getDayOfMonth()){
                todayActivities.add(a);
            }
            else throw new SecurityActivityException("There is now activities from today to show");
        }
        return todayActivities;
    }

    @RequestMapping(value = "/fromDate",method = RequestMethod.GET)
    @ResponseBody List<Activity> findAllFromDate(@RequestParam int day, @RequestParam int month, @RequestParam int year){
        LocalDate localDate = LocalDate.of(year,month,day);
        List<Activity> listOfUserActivities = activityRepo.findAllByUser(discoveryService.getLoggedUser().getUsername());
        List<Activity> listOfFinded = new ArrayList<>();
        for (Activity a:listOfUserActivities) {
            LocalDate activityDate = a.getDateCreated().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            if(activityDate.getDayOfMonth() == localDate.getDayOfMonth() &&
                    activityDate.getMonth() == localDate.getMonth() &&
                    activityDate.getYear() == localDate.getYear()){

                listOfFinded.add(a);
            }
        }
        return listOfFinded;
    }
}
