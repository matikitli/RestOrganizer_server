package com.controllers.activity;


import com.exceptions.SecurityActivityException;
import com.models.Activity;
import com.models.UserObject;
import com.models.enums.TypeEnum;
import com.repositories.ActivityRepo;
import com.security.service.DiscoveryService;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/activity")
public class ActivityWriterController {


    private final ActivityRepo activityRepo;
    private final DiscoveryService discoveryService;


    @Autowired
    public ActivityWriterController(DiscoveryService discoveryService,ActivityRepo activityRepo){
        this.activityRepo=activityRepo;
        this.discoveryService=discoveryService;
    }


    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    Activity addNewActivity(
            @ApiParam(value = "Short  title of activity") @RequestParam String title,
            @ApiParam(value = "tag of your activity for easier manage") @RequestParam TypeEnum typeEnumString,
            @ApiParam(value = "describe your activity") @RequestParam String description,
            @ApiParam(value = "(1-10) rate how you feel about it") @RequestParam int rate,
            @ApiParam(value = "how many minutes did it take you") @RequestParam int minutes,
            @ApiParam(value = "add useful links") @RequestParam String link,
            @ApiParam(value = "(0-100)% how many u learned of what u were doing ") @RequestParam int learned){
        Activity activity = new Activity(discoveryService.getLoggedUser().getUsername(),title, typeEnumString,description,rate,minutes,link,learned);
        activity.setCreator_id(discoveryService.getLoggedUser().getId());
        activityRepo.save(activity);
        return activity;
    }

    @RequestMapping(value = "/{n}/edit",method = RequestMethod.PUT)
    @ResponseBody
    Activity editActivityById(@PathVariable("n") Long id, @ApiParam(value = "Short  title of activity") @RequestParam(required = false) String title,
                              @ApiParam(value = "tag of your activity for easier manage") @RequestParam(required = false) TypeEnum typeEnumString,
                              @ApiParam(value = "describe your activity") @RequestParam(required = false) String description,
                              @ApiParam(value = "(1-10) rate how you feel about it") @RequestParam(required = false) int rate,
                              @ApiParam(value = "how many minutes did it take you") @RequestParam(required = false) int minutes,
                              @ApiParam(value = "add useful links") @RequestParam(required = false) String link,
                              @ApiParam(value = "(0-100)% how many u learned of what u were doing ") @RequestParam(required = false) int learned) throws SecurityActivityException {
        UserObject userObject=discoveryService.getLoggedUser();
                Activity activity = activityRepo.findOne(id);
                if(userObject.isOwnerOfActivity(activity)) {
                    activity.setTitle(title);
                    activity.setDescription(description);
                    activity.setTypeEnum(typeEnumString);
                    activity.setRate(rate);
                    activity.setMinutes(minutes);
                    activity.setLink(link);
                    activity.setPercentLearned(learned);
                    activityRepo.save(activity);
                    return activity;
                }
                else throw new SecurityActivityException("U dont have permission to edit this activity");

    }

    @RequestMapping(value = "/{n}/del",method = RequestMethod.DELETE)
    @ResponseBody Activity deleteActivityById(@PathVariable("n") Long id)throws SecurityActivityException{
                UserObject userObject=discoveryService.getLoggedUser();
                Activity activity=activityRepo.findOne(id);
                if(userObject.isOwnerOfActivity(activity)){
                    activityRepo.delete(id);
                    return activity;
                }
                else throw new SecurityActivityException("U dont have permission to delete this activity");
    }



}
