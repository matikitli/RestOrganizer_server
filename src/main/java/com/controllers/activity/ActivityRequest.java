package com.controllers.activity;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.models.Activity;
import com.models.enums.TypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ApiModel(value = "Activity ")
@JsonDeserialize(builder = ActivityRequest.Builder.class)
public class ActivityRequest {

    @ApiModelProperty(value = "Title in all format",required = true,example = "random title")
    @NotNull
    @Size(min=1)
    private String title;

    @ApiModelProperty(value = "Description, not required",required = false,example = "I was doing smth")
    private String description;

    @ApiModelProperty(value = "Tag of your activity",required = false,example = "JAVA")
    private TypeEnum typeEnum;

    @ApiModelProperty(value = "Rate in number from 1 to 10",required = true,example = "7")
    @NotNull
    @Min(1) @Max(10)
    private int rate;

    @ApiModelProperty(value = "Minutes in minutes :P (int)",required = true,example = "66")
    @NotNull
    @Min(1)
    private int minutes;

    @ApiModelProperty(value = "Learned (0-100)",required = true,example = "85")
    @NotNull
    @Min(0) @Max(100)
    private int learned;

    @ApiModelProperty(value = "Link to some stuff",required = false,example = "www.kupa-dupa.pl")
    @Min(1)
    private String link;

    @ApiModelProperty(value = "Username of creator",required = true,example = "klaudia69")
    @NotNull
    @Min(1)
    private String username;

    private Builder build;

    public ActivityRequest(Builder builder){

        this.title=builder.title;
        this.rate=builder.rate;
        this.minutes=builder.minutes;
        this.learned=builder.learned;
        this.build=builder;
    }

    public Builder getBuild() {
        return build;
    }

    public void setBuild(Builder build) {
        this.build = build;
    }

    @JsonPOJOBuilder(buildMethodName = "create", withPrefix = "set")
    public static final class Builder {
        private String title;
        private int rate;
        private int minutes;
        private int learned;



        private String description;
        private String link;
        private String user;
        private TypeEnum typeEnum;

        public Builder(String title,int rate,int minutes,int learned) {
            this.title=title;
            this.rate=rate;
            this.minutes=minutes;
            this.learned=learned;
        }
        public Builder setTitle(String title){
            this.title=title;
            return this;
        }
        public Builder setRate(int rate){
            this.rate=rate;
            return this;
        }
        public Builder setMinutes(int minutes){
            this.minutes=minutes;
            return this;
        }
        public Builder setLearned(int learned){
            this.learned=learned;
            return this;
        }
        public Builder setDescription(String description){
            this.description=description;
            return this;
        }
        public Builder setLink(String link){
            this.link=link;
            return this;
        }
        public Builder setUser(String user){
            this.user=user;
            return this;
        }
        public Builder setTypeEnum(TypeEnum typeEnum){
            this.typeEnum=typeEnum;
            return this;
        }
        public ActivityRequest create(){return new ActivityRequest(this);}
    }


    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public TypeEnum getTypeEnum() {
        return typeEnum;
    }

    public int getRate() {
        return rate;
    }

    public int getMinutes() {
        return minutes;
    }

    public int getLearned() {
        return learned;
    }

    public String getLink() {
        return link;
    }

    public String getUsername() {
        return username;
    }

    public Activity toActivity(){
        return new Activity(username,title,typeEnum,description,rate,minutes,link,learned);
    }
}
