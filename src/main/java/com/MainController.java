package com;

import com.models.Activity;
import com.models.enums.TypeEnum;
import com.repositories.ActivityRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
//@Api(value = "MainGreeting", description = "Main Greeting")
public class MainController {

    @Autowired
    private ActivityRepo activityRepo;

@RequestMapping(path = "/hello", method = RequestMethod.GET)
    public @ResponseBody String greeting(){

    return "Hello new customer";
}

@RequestMapping(value = "/api/v1", method = RequestMethod.POST)
    public @ResponseBody String add (@RequestParam String name){
        return "HI: "+name;
}


}
