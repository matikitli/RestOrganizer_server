package com.firebase.push;


import com.firebase.AndroidPushNotificationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.CompletableFuture;

@Component
public class ScheduledPush {

    @Autowired
    AndroidPushNotificationsService androidPushNotificationsService;

    private final String TOPIC = "JavaSampleApproach";

    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Scheduled(fixedRate = 1000*60*60*60) //1000=SEC  || sec*60*60=HOUR xd xd xd
    public void sendPushTest(){
        JSONPushObject body = new JSONPushObject();
        body.setTopic("WOO JAKIE DOBRE KURWA");
        body.setTitle("Napierdala sobie push");
        body.setBody("JAKIE TO KURWA PYSZNE I DOBRE JPRDL!\n"+dateFormat.format(new Date()));


        CompletableFuture<String> pushNotification = androidPushNotificationsService.send(body.make());
        CompletableFuture.allOf(pushNotification).join();

        System.out.print("Send push: "+ dateFormat.format(new Date())+" \n");
    }
}
