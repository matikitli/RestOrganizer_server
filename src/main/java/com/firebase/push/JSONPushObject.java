package com.firebase.push;

import org.json.simple.JSONObject;
import org.springframework.http.HttpEntity;

import java.text.SimpleDateFormat;



public class JSONPushObject{

    private String topic;
    private String title;
    private String body;
    JSONObject object = new JSONObject();
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    public JSONPushObject(){
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public JSONObject getObject() {
        return object;
    }

    public void setObject(JSONObject object) {
        this.object = object;
    }

    public HttpEntity<String> make(){

        object.put("to", "/topics/"+"JavaSampleApproach");
        object.put("priority", "high");

        JSONObject notification = new JSONObject();
        notification.put("title", getTitle());
        notification.put("body", getBody());

        JSONObject data = new JSONObject();
        data.put("Key-1", "JSA Data 1");
        data.put("Key-2", "JSA Data 2");

        object.put("notification", notification);
        object.put("data", data);

        return new HttpEntity<>(object.toString());
    }
}
